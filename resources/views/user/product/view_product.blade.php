@extends('user.layouts.layout')

@section('content')

        <!-- Begin Page Content -->
        <div class="container-fluid">
            @if(session()->get('success'))
              <div class="alert alert-success">
                {{ session()->get('success') }}  
              </div><br />
            @endif
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Products</h1>
          <?php

              $product_size = $products->total();
              if(Session::get('store')['product_limit'] == "Unlimited") {
                $product_label = "Unlimited";
                $product_left = 1;
              }
              else {
                $product_left = Session::get('store')['product_limit'] - $product_size;
                $product_label = ($product_left == 0) ? "full" : $product_left." left";
              }
          ?>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Product: {{$product_label}}</h6>
                  <div class="text-right">
                    @if(($product_left > 0) || ($product_label == "Unlimited")) 
                      <a class="btn btn-primary" href="{{ route('products.create') }}">
                        {{ __('Add Product') }}
                      </a>
                    @else 
                      <a class="btn btn-success" href="#full">
                        {{ __('Product Full') }}
                      </a>
                    @endif
                  </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  @if($product_size > 0)
                  <thead align="center">
                    <tr>
                      <th>No.</th>
                      <th>Name</th>
                      <th>Category</th>
                      <th>Amount</th>
                      <th>Buy in Price</th>
                      <th>Sell out Price</th>
                      <th>Image</th>
                      <th colspan="2">Action</th>
                    </tr>
                  </thead>
                  <tbody align="center">
                    <?php 
                        $count = $products->firstItem();
                    ?>
                      @foreach($products as $product)
                        <tr>
                          <td>{{ $count++ }}</td>
                          <td>{{ $product->name }}</td>
                          <td>{{ $product->category }}</td>
                          <td>{{ $product->amount }}</td>
                          <td>${{ $product->price_in }} per {{ $product->measure_unit }}</td>
                          <td>${{ $product->price_out }} per {{ $product->measure_unit }}</td>
                          <td>
                            <img src="{{url('/product/image/'.$product->image_path)}}" 
                                class="rounded" height="70px" alt="...">
                          </td>
                          <td>
                              <a class="btn btn-warning" href="{{ route('products.edit', $product->id) }}">
                                  {{ __('Update') }}
                              </a>
                          </td>
                          <td>
                              <form action="{{ route('products.destroy', $product->id) }}" method="POST">
                                  @method('DELETE')
                                  @csrf
                                  <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')">
                                      {{ __('Delete') }}
                                  </button>
                              </form>
                          </td>
                        </tr>
                      @endforeach
                      @while(($count-1) % 5 != 0)
                      <tr>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                      </tr>
                      <?php $count++ ?>
                      @endwhile
                  </tbody>
                  @else
                      <p>You have 0 product!</p>
                  @endif
                </table>
                  <div class="text-right">
                      {{ $products->onEachSide(5)->links() }}
                  </div>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->
@endsection