<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', 'AuthController@login');
Route::post('register', 'AuthController@register');
Route::get('plans', 'PlanPriceController@apiShowAll');


Route::middleware('auth:api')->group(function () {
    Route::get('logout', 'AuthController@logout');
    Route::get('user', 'AuthController@user');

        
        // Route::post('image', 'ImageController@apiSaveImage');

    Route::post('contact/showAll', 'ContactController@apiShowAll');
    Route::get('contact/{contact}', 'ContactController@apiShow');
    Route::post('contact', 'ContactController@apiAdd');
    Route::post('contact/update', 'ContactController@apiUpdate');
    Route::delete('contact/delete', 'ContactController@apiDelete');

    Route::post('category/showAll', 'CategoryController@apiShowAll');
    Route::get('category/{category}', 'CategoryController@apiShow');
    Route::post('category', 'CategoryController@apiAdd');
    Route::post('category/update', 'CategoryController@apiUpdate');
    Route::delete('category/delete', 'CategoryController@apiDelete');

    Route::post('product/showAll', 'ProductController@apiShowAll');
    Route::get('product/{product}', 'ProductController@apiShow');
    Route::get('product/showByCategory/{category}', 'ProductController@apiShowByCategory');
    Route::post('product', 'ProductController@apiAdd');
    Route::get('product/image/{image}', 'ProductController@apiShowImage');
    Route::post('product/update', 'ProductController@apiUpdate');
    Route::delete('product/delete', 'ProductController@apiDelete');

    Route::post('transaction/showAll', 'TransactionController@apiShowAll');
    Route::get('transaction/{transaction}', 'TransactionController@apiShow');
    Route::post('transaction/showSaleList', 'TransactionController@apiShowSaleList');
    Route::post('transaction/showPurchaseList', 'TransactionController@apiShowPurchaseList');
    Route::post('transaction', 'TransactionController@apiAdd');
        // Route::post('transaction/update', 'TransactionController@apiUpdate');
    Route::delete('transaction/delete', 'TransactionController@apiDelete');



    Route::get('/store/{store}', 'StoreController@apiShow');
    Route::post('/store', 'StoreController@apiAdd');

        // Route::get('/transaction/{transaction}', 'TransactionController@apiShow');
        // Route::post('/transaction', 'TransactionController@apiAdd');

});

