@extends('user.layouts.layout')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Reports</h1>
  <p class="mb-4">
    Reports related with last 6 months performance in a summery form of store revenue and sale growth.
  </p>

  <!-- Content Row -->
  <div class="row">

    <div class="col-xl-8 col-lg-7">

      <!-- Area Chart -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Sale Performance Graph</h6>
        </div>
        <div class="card-body">
          <div class="chart-area">
            <canvas id="myAreaChart"></canvas>
          </div>
          <hr>
          This is a graph showing about store revenue from selling.
        </div>
      </div>

      <!-- Bar Chart -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Sale Report</h6>
        </div>
        <div class="card-body">
          <div class="chart-bar">
            <canvas id="myBarChart"></canvas>
          </div>
          <hr>
          This is a summery of last 6 months performance.
        </div>
      </div>

    </div>

    <!-- Donut Chart -->
    <div class="col-xl-4 col-lg-5">
      <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Inventory</h6>
        </div>
        <!-- Card Body -->
        <div class="card-body">
          <div class="chart-pie pt-4">
            <canvas id="myPieChart"></canvas>
          </div>
          <hr>
          <a href="#" class="btn btn-primary btn-circle btn-sm">
          </a>
          Product on Stock <code>55%</code>
          <div class="my-2"></div>
          <a href="#" class="btn btn-success btn-circle btn-sm">
          </a>
          Product out Stock <code>30%</code>
          <div class="my-2"></div>
          <a href="#" class="btn btn-info btn-circle btn-sm">
          </a>
          Free Stock <code>15%</code>
        </div>
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
@endsection