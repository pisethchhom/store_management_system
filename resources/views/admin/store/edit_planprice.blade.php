@extends('auth.layouts.layout')

@section('content')
<div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">{{ __('Update Price Plan!') }}</h1>
                  </div>
                  <form method="POST" action="{{ route('plan_prices.update', $price->id) }}" class="user">
                        @csrf
                        @method('PATCH')
                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" name="title"
                         id="exampleFirstName" placeholder="Plan Title" value="{{ $price->title }}" required autofocus>
                    </div>

                    <div class="form-group">
                        <input type="number" min="0" class="form-control" name="price"
                         id="exampleFirstName" placeholder="Plan Price" value="{{ $price->price }}" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" name="category"
                         id="exampleFirstName" placeholder="Category Limited" value="{{ $price->category }}" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" name="product"
                         id="exampleFirstName" placeholder="Product Limited" value="{{ $price->product }}" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" name="report"
                         id="exampleFirstName" placeholder="Report Limited" value="{{ $price->report }}" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" name="button_label"
                         id="exampleFirstName" placeholder="Label display on button" value="{{ $price->button_label }}" required>
                    </div>
                    
                    <div class="form-group">
                      <textarea rows="4" name="description" class="form-control" placeholder="Plan Description...">{{ $price->description }}</textarea>
                    </div>
                    
                    <hr>

                    <button type="submit" class="btn btn-primary btn-user btn-block">
                        {{ __('Update') }}
                    </button>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    
      </div>
@endsection
