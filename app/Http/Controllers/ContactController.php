<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Http\Resources\ContactResource;
use Illuminate\Http\Request;
use Session;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::where('contacts.store_id','=',Session::get('store')['store_id'])->paginate(5);

        return view('user/contact/view_contact', compact('contacts'));
    }

    public function apiShowAll(Request $request)
    {
        $request->validate([
            'store_id'=>'required',
        ]);

        $contacts = Contact::where('store_id','=',$request->get('store_id'))->get();

        return response()->json([
            'response' => 1,
            'data' => $contacts,
        ], 200);
    }

    public function apiShow($id)
    {
        $contact = Contact::where('id','=',$id)->get();

        return response()->json([
            'response' => 1,
            'data' => $contact,
        ], 200);
    }

    public function apiAdd(Request $request) {
        $request->validate([
            'name'=>'required',
            'store_id'=>'required',
        ]);
          
        $contact = new contact([
            'name' => $request->get('name'),
            'role' => $request->get('role'),
            'store_id' => $request->get('store_id'),
            'telephone' => $request->get('telephone'),
            'description' => $request->get('description'),
        ]);
          
        $contact->save();

        return response()->json([
            'response' => 1,
            'message' => 'Contact has been added',
            'data' => $contact,
        ], 200);
    }

    public function apiUpdate(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'name' => 'required',
            'store_id' => 'required',
        ]);
        
        $contact = Contact::find($request->get('id'));

        if ($contact) {
            if ($contact->store_id != $request->get('store_id')) {
                return response()->json([
                    'response' => 0,
                    'message' => 'Invalid store id',
                ], 401);
            }

            $contact->name = $request->get('name');
            $contact->role = $request->get('role');
            $contact->telephone = $request->get('telephone');
            $contact->description = $request->get('description');
            $contact->save();

            return response()->json([
                'response' => 1,
                'message' => 'Contact has been updated',
                'data' => $contact,
            ], 200);
        }
        else {
            return response()->json([
                'response' => 0,
                'message' => 'Invalid contact id',
            ], 401);
        }
    }

    public function apiDelete(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'store_id' => 'required',
        ]);
        
        $contact = Contact::find($request->get('id'));
        
        if ($contact) {
            if ($contact->store_id != $request->get('store_id')) {
                return response()->json([
                    'response' => 0,
                    'message' => 'Invalid store id',
                ], 401);
            }

            $contact->delete();
   
            return response()->json([
                'response' => 1,
                'message' => 'Contact has been deleted',
            ], 200);
        }
        else {
            return response()->json([
                'response' => 0,
                'message' => 'Invalid contact id',
            ], 401);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user/contact/add_contact');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
          ]);
          
        $contact = new contact([
            'name' => $request->get('name'),
            'role' => $request->get('role'),
            'store_id' => Session::get('store')['store_id'],
            'telephone' => $request->get('telephone'),
            'description' => $request->get('description'),
            ]);
        $contact->save();
        return redirect('contacts')->with('success', 'Contact has been added');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact= Contact::find($id);

        return view('user/contact/edit_contact', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
        ]);
    
        $contact = Contact::find($id);
        $contact->name = $request->get('name');
        $contact->role = $request->get('role');
        $contact->telephone = $request->get('telephone');
        $contact->description = $request->get('description');
        $contact->save();
    
        return redirect('contacts')->with('success', 'Contact has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Contact::find($id);
        $contact->delete();
   
        return redirect('contacts')->with('success', 'Contact has been deleted Successfully');
    }
}
