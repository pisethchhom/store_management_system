<?php
  use App\PlanPricing;
  $plan = PlanPricing::where('id','=', $plan)->select('id', 'title')->get();
  $plan = $plan->toArray()[0]
?>
@extends('auth.layouts.layout')

@section('content')
<div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">{{ __('Create Your Store!') }}</h1>
                  </div>
                  <form method="POST" action="{{ route('stores.store') }}" class="user">
                        @csrf
                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" name="name"
                         id="exampleFirstName" placeholder="Store Name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                    </div>

                    <div class="form-group">
                      <textarea rows="4" cols="50" name="description" class="form-control" placeholder="Store Description..."></textarea>
                    </div>
                    
                    <div class="form-group">
                      <p>Package: {{  $plan['title'] }}</p>
                    </div>
                    <input type="hidden" name="user_id" value="{{ (Auth::id()) }}">
                    <input type="hidden" name="plan_id" value="{{ $plan['id'] }}">
                    <hr>

                    <button type="submit" class="btn btn-primary btn-user btn-block">
                        {{ __('Finish') }}
                    </button>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    
      </div>
@endsection
