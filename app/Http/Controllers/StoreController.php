<?php

namespace App\Http\Controllers;

use App\Store;
use App\Http\Resources\StoreResource;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function index()
    {
        $stores = Store::join('plan_pricings', 'stores.plan_id', '=', 'plan_pricings.id')
                        ->join('users', 'stores.user_id', '=', 'users.id')
                        ->select('stores.id','stores.name as store_title','stores.description','stores.expire_plan','stores.isActive',
                        'plan_pricings.title as plan_title', 'users.name as username')
                        ->paginate(5);

        return view('admin/store/view_store', compact('stores'));
    }

    public function apiShow(Store $store)
    {
        return response()->json([
            'response' => 1,
            'data' => $store,
        ], 200);
    }

    public function apiAdd(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'plan_id' => 'required'
          ]);

        $store = new store([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'user_id' => auth()->user()->id,
            'plan_id' => (int)$request->get('plan_id'),
            ]);
        $store->save();

        return response()->json([
            'response' => 1,
            'message' => 'Store has been added',
            'data' => $store,
        ], 200);
    }

    public function chooseStore() 
    {
        return view('guest/store/choose_store');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $plan = (int)$request->get('plan_id');
        return view('guest/store/create_store', compact('plan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
          ]);

        $store = new store([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'user_id' => (int)$request->get('user_id'),
            'plan_id' => (int)$request->get('plan_id'),
            ]);
        $store->save();
        return redirect('home')->with('success', 'Welcome to your store!');
    }

    public function activate($id)
    {
        $store = Store::find($id);
        $store->isActive = 1;
        $store->save();

        if(auth()->user()->isAdmin == 1){

            return redirect('stores')->with('success', 'Store has been activated');
            
        }
        return view('user.dashboard');
    }

    public function deactivate($id)
    {
        $store = Store::find($id);
        $store->isActive = 0;
        $store->save();

        return redirect('stores')->with('success', 'Store has been deactivated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $store = Store::find($id);
        $store->delete();
   
        return redirect('home')->with('success', 'Product has been deleted Successfully');
    }
}
