<?php

namespace App\Http\Controllers;

use App\User;
use App\Store;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function register(Request $request) {
        $validatedData = $request->validate([
            'name'=>'required|max:55',
            'email'=>'email|required|unique:users',
            'password'=>'required|confirmed'
        ]);

        $validatedData['password'] = bcrypt($request->password);

        $user = User::create($validatedData);

        $accessToken = $user->createToken('authToken')->accessToken;

        return response()->json([
            'response' => 1,
            'user' => $user,
            'access_token' => $accessToken
        ], 201);
    }

    public function login(Request $request) {
        $loginData = $request->validate([
            'email'=>'email|required',
            'password'=>'required'
        ]);

        if (!auth()->attempt($loginData)) {
            return response()->json([
                'response' => 0,
                'message' => 'Invalid credentials'
            ], 401);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        $store = Store::where('user_id','=', auth()->user()->id)->get();

        return response()->json([
            'response' => 1,
            'user' => auth()->user(),
            'store' => $store,
            'access_token' => $accessToken
        ], 200);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();        
        
        return response()->json([
            'response' => 1,
            'message' => 'Successfully logged out'
        ]);
    }

    public function user(Request $request)
    {   
        return response()->json([
            'response' => 1,
            'user' => $request->user(),
        ]);
    }
}
