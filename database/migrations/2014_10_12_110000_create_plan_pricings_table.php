<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanPricingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_pricings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('price');
            $table->string('title',100);
            $table->string('category',100);
            $table->string('product',100);
            $table->string('report',100);
            $table->string('description',100);
            $table->string('button_label',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_pricings');
    }
}
