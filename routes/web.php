<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('generate-pdf','TransactionInfoController@generatePDF');

Route::get('/', function () {
    return view('guest/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// new
// Route::post('/register', 'AuthController@register');
// Route::post('/login', 'AuthController@login');

Route::get('/admin', function () {
    if (Auth::check()) {
        return redirect()->action('HomeController@index');
    }
    return view('auth.admin.login');
});

Route::get('/guest', function () {
    return view('guest/home');
});
Route::get('/user/chooseStore', 'StoreController@chooseStore')->name('chooseStore');

Route::get('/plan_price/json', 'PlanPriceController@listPrice');

// Route::get('/guest/store', function () {
//     return view('guest/store/choose_store');
// });
// Route::get('/guest/store/create', function () {
//     return view('guest/store/create_store');
// });

// user middleware
Route::middleware('auth')->group(function () {
    Route::get('/user', function () {
        
    });
    Route::get('/user/inventory', function () {
        return view('user/inventory');
    });
    Route::get('/user/report', function () {
        return view('user/report');
    });
    Route::resource('stores', 'StoreController');
    Route::get('stores/{id}/activate', 'StoreController@activate');
    Route::resource('categories', 'CategoryController');
    Route::resource('products', 'ProductController');
    Route::get('product/image/{image}', 'ProductController@apiShowImage');
    
    Route::resource('contacts', 'ContactController');
    Route::resource('transactions', 'TransactionController');
    Route::resource('transaction_infos', 'TransactionInfoController');
    // Route::get('search','ProductController@search');
    
    // admin middleware
    Route::middleware('admin')->group(function () {
        Route::get('/admin/home', function () {
            return view('admin.dashboard');
        });
        Route::resource('plan_prices', 'PlanPriceController');
        Route::get('stores/{id}/deactivate', 'StoreController@deactivate');

        Route::resource('users', 'UserController');
        Route::get('users/{id}/activate', 'UserController@activate');
        Route::get('users/{id}/deactivate', 'UserController@deactivate');
    });
});

