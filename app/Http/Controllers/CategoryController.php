<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Resources\CategoryResource;
use Illuminate\Http\Request;
use Session;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::where('store_id','=',Session::get('store')['store_id'])->paginate(5);
        // $users = DB::table('users')
            
        return view('user/category/view_category', compact('categories'));
    }

    public function apiShowAll(Request $request)
    {
        $request->validate([
            'store_id'=>'required',
        ]);

        $categories = Category::where('store_id','=',$request->get('store_id'))->get();

        return response()->json([
            'response' => 1,
            'data' => $categories,
        ], 200);
    }

    public function apiShow($id)
    {
        $category = Category::where('id','=',$id)->get();

        return response()->json([
            'response' => 1,
            'data' => $category,
        ], 200);
    }

    public function apiAdd(Request $request) {
        $request->validate([
            'name'=>'required',
            'store_id'=>'required',
        ]);

        $category = new Category([
            'name' => $request->get('name'),
            'description'=> $request->get('description'),
            'store_id' => $request->get('store_id')
          ]);
          
        $category->save();

        return response()->json([
            'response' => 1,
            'message' => 'Category has been added',
            'data' => $category,
        ], 200);
    }

    public function apiUpdate(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'name' => 'required',
            'store_id' => 'required',
        ]);
        
        $category = Category::find($request->get('id'));

        if ($category) {
            if ($category->store_id != $request->get('store_id')) {
                return response()->json([
                    'response' => 0,
                    'message' => 'Invalid store id',
                ], 401);
            }

            $category->name = $request->get('name');
            $category->description = $request->get('description');
            $category->save();

            return response()->json([
                'response' => 1,
                'message' => 'Category has been updated',
                'data' => $category,
            ], 200);
        }
        else {
            return response()->json([
                'response' => 0,
                'message' => 'Invalid category id',
            ], 401);
        }
    }

    public function apiDelete(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'store_id' => 'required',
        ]);
        
        $category = Category::find($request->get('id'));
        
        if ($category) {
            if ($category->store_id != $request->get('store_id')) {
                return response()->json([
                    'response' => 0,
                    'message' => 'Invalid store id',
                ], 401);
            }

            $category->delete();
   
            return response()->json([
                'response' => 1,
                'message' => 'Category has been deleted',
            ], 200);
        }
        else {
            return response()->json([
                'response' => 0,
                'message' => 'Invalid category id',
            ], 401);
        }
    }

    public function listJson($store_id) 
    {
        $categories =  Category::where('store_id','=',$store_id); 
        $categories_json = json_encode($categories);
        return $categories_json;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user/category/add_category');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
          ]);
        $category = new Category([
            'name' => $request->get('name'),
            'description'=> $request->get('description'),
            'store_id' => Session::get('store')['store_id']
          ]);
        $category->save();
        return redirect('categories')->with('success', 'Category has been added');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category= Category::find($id);

        return view('user/category/edit_category', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
          ]);
    
        $category = Category::find($id);
        $category->name = $request->get('name');
        $category->description = $request->get('description');
        $category->save();
    
        return redirect('categories')->with('success', 'Category has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
   
        return redirect('categories')->with('success', 'Category has been deleted Successfully');
    }
}
