<script src="{{ asset('theme_cohost/js/jquery.min.js') }}"></script>
<script src="{{ asset('theme_cohost/js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ asset('theme_cohost/js/popper.min.js') }}"></script>
<script src="{{ asset('theme_cohost/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('theme_cohost/js/jquery.easing.1.3.js') }}"></script>
<script src="{{ asset('theme_cohost/js/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('theme_cohost/js/jquery.stellar.min.js') }}"></script>
<script src="{{ asset('theme_cohost/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('theme_cohost/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('theme_cohost/js/aos.js') }}"></script>
<script src="{{ asset('theme_cohost/js/jquery.animateNumber.min.js') }}"></script>
<script src="{{ asset('theme_cohost/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('theme_cohost/js/jquery.timepicker.min.js') }}"></script>
<script src="{{ asset('theme_cohost/js/scrollax.min.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
<script src="{{ asset('theme_cohost/js/google-map.js') }}"></script>
<script src="{{ asset('theme_cohost/js/main.js') }}"></script>