    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('theme2/theme_stock_management/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('theme2/theme_stock_management/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  
    <!-- Core plugin JavaScript-->
    <script src="{{ asset('theme2/theme_stock_management/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
  
    <!-- Custom scripts for all pages-->
    <script src="{{ asset('theme2/theme_stock_management/js/sb-admin-2.min.js') }}"></script>
  
    <!-- Page level plugins -->
    <script src="{{ asset('theme2/theme_stock_management/vendor/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('theme2/theme_stock_management/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
  
    <!-- Page level custom scripts -->
    <script src="{{ asset('theme2/theme_stock_management/js/demo/chart-area-demo.js') }}"></script>
    <script src="{{ asset('theme2/theme_stock_management/js/demo/chart-pie-demo.js') }}"></script>
    <script src="{{ asset('theme2/theme_stock_management/js/demo/chart-bar-demo.js') }}"></script>
    <script src="{{ asset('theme2/theme_stock_management/js/demo/datatables-demo.js') }}"></script>
  


  