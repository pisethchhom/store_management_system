<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'category_id',
        'amount',
        'price_in',
        'price_out',
        'measure_unit',
        'image_path',
        'store_id'
      ];
}
