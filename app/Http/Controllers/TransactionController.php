<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Transaction_info;
use App\Product;
use App\Contact;

use App\Http\Resources\TransactionResource;
use App\Http\Resources\TransactionInfoResource;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Log;


class TransactionController extends Controller
{
/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::where('transactions.store_id','=',Session::get('store')['store_id'])
                            ->leftjoin('contacts', 'transactions.contact_id', '=', 'contacts.id')
                            ->select('transactions.*',
                            'contacts.name as contact')
                            ->paginate(5);

        return view('user/transaction/view_transaction', compact('transactions'));
    }

    public function apiShowAll(Request $request)
    {
        $request->validate([
            'store_id'=>'required',
        ]);

        $transactions = Transaction::where('store_id','=',$request->get('store_id'))
                            ->get()->toArray();

        if ($transactions) {
            for ($i=0; $i<sizeof($transactions); $i++) {
                $contact = Contact::find($transactions[$i]['contact_id']);
                
                $transaction_infos = Transaction_info::where('transaction_id', '=', $transactions[$i]['id'])
                                ->leftjoin('products', 'transaction_infos.product_id', '=', 'products.id')
                                ->select('products.*', 'transaction_infos.*')
                                ->get();

                $transactions[$i]['contact_id'] = $contact;
                $transactions[$i]['transaction_info'] = $transaction_infos;
            }
        }

        return response()->json([
            'response' => 1,
            'data' => $transactions,
        ], 200);
    }

    public function apiShow($id)
    {
        $transaction = Transaction::where('id','=',$id)->get();
        $contact = Contact::find($transaction['contact_id']);

        $transaction_infos = Transaction_info::where('transaction_id', '=', $id)
                            ->join('products', 'transaction_infos.product_id', '=', 'products.id')
                            ->select('products.*', 'transaction_infos.*')
                            ->get();
                            
        $transaction['contact_id'] = $contact;
        $transaction['transaction_info'] = $transaction_infos;
        
        return response()->json([
            'response' => 1,
            'data' => $transaction,
        ], 200);
    }

    function apiShowByType($store_id, $is_sell)
    {
        $transactions = Transaction::where('store_id','=',$store_id)
                            ->where('isSell', '=', $is_sell)
                            ->get()->toArray();
        if ($transactions) {
            for ($i=0; $i<sizeof($transactions); $i++) {
                $contact = Contact::find($transactions[$i]['contact_id']);
                
                $transaction_infos = Transaction_info::where('transaction_id', '=', $transactions[$i]['id'])
                                ->join('products', 'transaction_infos.product_id', '=', 'products.id')
                                ->select('products.*', 'transaction_infos.*')
                                ->get();

                $transactions[$i]['contact_id'] = $contact;
                $transactions[$i]['transaction_info'] = $transaction_infos;
            }
        }

        return response()->json([
            'response' => 1,
            'data' => $transactions,
        ], 200);
    }

    public function apiShowSaleList(Request $request)
    {
        $request->validate([
            'store_id'=>'required',
        ]);

        return $this->apiShowByType($request->get('store_id'), true);
    }

    public function apiShowPurchaseList(Request $request)
    {
        $request->validate([
            'store_id'=>'required',
        ]);

        return $this->apiShowByType($request->get('store_id'), false);
    }

    public function apiAdd(Request $request) 
    {
        $request->validate([
            'product_id' => 'required',
            'product_amount' => 'required',
            'store_id' => 'required',
            'total_price' => 'required',
        ]);

        $is_sell = $request->get('options') == "sell_out";
        $total_price = $request->get('total_price');
        $contact_id = $request->get('contact_id');
        $store_id = $request->get('store_id');

        $product_id = $request->get('product_id');
        $product_amount = $request->get('product_amount');
        $product = Product::find($product_id);

        if ($product) {
            if ($product->store_id == $store_id) {
                if ($contact_id != null) {
                    $contact = Contact::find($contact_id);
                    if (!$contact) {
                        return response()->json([
                            'response' => 0,
                            'message' => 'Invalid contact id',
                        ], 401);
                    }
                }

                if ($is_sell) {
                    if ($product->amount < $product_amount) {
                        return response()->json([
                            'response' => 0,
                            'message' => 'Invalid product amoung',
                        ], 401);
                    }
                    $product->amount = $product->amount - $product_amount;
                }
                else {
                    $product->amount = $product->amount + $product_amount;
                }
                $product->save();

                $transaction = new Transaction([
                    'isSell' => $is_sell,
                    'total_price' => $total_price,
                    'contact_id' => $contact_id,
                    'store_id' => $store_id,
                ]);
                $transaction->save();
                $transaction_id = $transaction->id;
                
                $transaction_info = new Transaction_info([
                    'amount' => $product_amount,
                    'product_id' => $product_id,
                    'transaction_id' => $transaction_id,
                    'store_id' => $store_id,
                ]);
                $transaction_info->save();

                return response()->json([
                    'response' => 1,
                    'message' => 'Transaction has been added',
                    'data' => $transaction,
                    'info' => $transaction_info,
                ], 200);
            }
        }

        return response()->json([
            'response' => 0,
            'message' => 'Invalid product id',
        ], 401);
    }

    public function apiDelete(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'store_id' => 'required',
        ]);
        
        $transaction = Transaction::find($request->get('id'));

        if ($transaction) {
            if ($transaction->store_id != $request->get('store_id')) {
                return response()->json([
                    'response' => 0,
                    'message' => 'Invalid store id',
                ], 401);
            }

            Transaction_info::where('transaction_id', '=', $request->get('id'))->delete();
            $transaction->delete();

            return response()->json([
                'response' => 1,
                'message' => 'Transaction has been deleted',
            ], 200);
        }

        return response()->json([
            'response' => 0,
            'message' => 'Invalid transaction id',
        ], 401);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user/transaction/add_transaction');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'product_name'=>'required',
            'product_amount'=>'required',
        ]);

        $is_sell = $request->get('options') == "sell_out";
        $store_id = Session::get('store')['store_id'];
        $contact_id = $request->get('contact_id');
        $total_price = $request->get('total_price');

        $product_name = $request->get('product_name');
        $product_amount = $request->get('product_amount');

        if (count($product_name) !== count(array_unique($product_name))) {
            return redirect()->back()->withErrors(['product can not be duplicate'])->withInput();
        }

        // store transaction
        // need: isSell(bool); total_price(int); contact_id(int); store_id(int)
        $transaction = new Transaction([
            'isSell' => $is_sell,
            'total_price' => $total_price,
            'contact_id' => $contact_id,
            'store_id' => $store_id,
        ]);
        $transaction->save();
        $transaction_id = $transaction->id;

        // loop and store transaction_info
        // need: amount(int); product_id(int); transaction_id(int); store_id(int)
        for ($i=0; $i<sizeof($product_name); $i++) {
            $product = json_decode($product_name[$i]);
            $pId = $product->id;
            $pAmount = $product_amount[$i];

            if ($is_sell) { // if buy in
                $product = Product::find($pId);
                $product->amount = $product->amount - $pAmount;
                $product->save();
            }
            else { // if buy in
                $product = Product::find($pId);
                $product->amount = $product->amount + $pAmount;
                $product->save();
            }

            $transaction_info = new Transaction_info([
                'amount' => $pAmount,
                'product_id' => $pId,
                'transaction_id' => $transaction_id,
                'store_id' => $store_id,
            ]);
            $transaction_info->save();
        }
        Log::info(print_r($product,true));

        return redirect('transactions')->with('success', 'Transaction has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = Transaction::where('transactions.id', '=', $id)
                            ->leftjoin('contacts', 'transactions.contact_id', '=', 'contacts.id')
                            ->select(
                                'transactions.id',
                                'transactions.created_at', 
                                'transactions.isSell',
                                'transactions.total_price',
                                'contacts.name as contact_name',
                                'contacts.role as contact_role',
                                'contacts.telephone as contact_phone')
                            ->get()->toArray()[0];

        $transaction_infos = Transaction_info::where('transaction_id', '=', $id)
                            ->join('products', 'transaction_infos.product_id', '=', 'products.id')
                            ->select(
                                'transaction_infos.amount as product_amount', 
                                'products.name as product_name',
                                'products.price_out as product_price')
                            ->get()->toArray();
                            

        return view('user/transaction/view_info_transaction', compact('transaction', 'transaction_infos'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaction = Transaction::find($id);

        Transaction_info::where('transaction_id', '=', $id)->delete();

        $transaction->delete();
   
        return redirect('transactions')->with('success', 'Transaction has been deleted Successfully');
    }
}
