<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
  <div class="container">
    <a class="navbar-brand" href="{{ url('guest') }}">Store Management System</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="oi oi-menu"></span> Menu
    </button>

    <div class="collapse navbar-collapse" id="ftco-nav">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active"><a href="{{ url('/guest') }}" class="nav-link">Home</a></li>
        <li class="nav-item"><a href="{{ asset('theme_cohost/about.html') }}" class="nav-link">About</a></li>
        {{-- <li class="nav-item"><a href="{{ asset('theme_cohost/domain.html') }}" class="nav-link">Domain</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ asset('theme_cohost/hosting.html') }}">Hosting</a></li>
        <li class="nav-item"><a href="{{ asset('theme_cohost/blog.html') }}" class="nav-link">Blog</a></li> --}}
        {{-- <li class="nav-item"><a href="{{ asset('theme_cohost/contact.html') }}" class="nav-link">Contact</a></li> --}}

        @if (Route::has('login'))
                    @auth
                        <li class="nav-item cta">
                          <a href="{{ url('/home') }}" class="nav-link">Home</a>
                        </li>
                    @else
                        <li class="nav-item cta">
                          <a href="{{ route('login') }}" class="nav-link">Login</a>
                        </li>
                    @endauth
            @endif
      </ul>
    </div>
  </div>
</nav>