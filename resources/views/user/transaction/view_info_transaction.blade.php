@extends('auth.layouts.layout')

@section('content')

<div class="container">
        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                    <div class="text-center">
                      <h1 class="h2 text-gray-700 mb-2">{{ __('Invoice') }}</h1>
                      <h1 class="h5 text-gray-700 mb-2">{{ __('#'.$transaction["id"]) }}</h1>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="h5 text-gray-800 mr-2">Type:</label>
                        <label class="text-gray-800">{{$transaction["isSell"] ? "Sell out" : "Buy in"}}</label>
                    </div>

                    <div class="form-group col-md-4">
                        <div class="row-md-4">
                            <label class="h5 text-gray-800 mr-2">{{$transaction["isSell"] ? "Billed from:" : "Billed To:" }}</label>
                            <label>{{$transaction["contact_name"] ? $transaction["contact_name"] : "(No Name)"}}</label>
                        </div>
                        <div>
                            <label>Role: {{$transaction["contact_role"] ? $transaction["contact_role"] : "(No Role)"}}</label>
                        </div>
                        <div>
                            <label>Phone: {{$transaction["contact_phone"] ? $transaction["contact_phone"] : "(No Phone)"}}</label>
                        </div>
                    </div>

                    <div class="form-group col-md-4">
                      <?php
                        $format = 'Y-m-d H:i:s';
                        $date = DateTime::createFromFormat($format, $transaction["created_at"]);
                      ?>
                        <label class="h5 text-gray-800 mr-2">Created Date:</label>
                        <label class="text-gray-800">{{$date->format('D-d-M-Y')}}</label>
                    </div>

                    <table class="table" style="table-layout: fixed;">
                        <thead align="center">
                          <tr class="heading-invoice">
                            <th scope="col">Product</th>
                            <th scope="col">Price</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Totals</th>
                          </tr>
                        </thead>
                        <tbody id="tbody_product" align="center">
                          @foreach ($transaction_infos as $item)
                            <tr>
                              <td>{{$item["product_name"]}}</td>
                              <td>${{$item["product_price"]}}</td>
                              <td>{{$item["product_amount"]}}</td>
                              <td scope="col">${{$item["product_price"] * $item["product_amount"]}}</td>
                            </tr>
                          @endforeach
                        </tbody>
                        <tfoot>
                          <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th scope="col">Total Price: ${{$transaction["total_price"]}}</th>
                          </tr>
                        </tfoot>
                    </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection