<?php

namespace App\Http\Controllers;

use App\PlanPricing;
use App\Http\Resources\PlanPriceResource;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Log;


class PlanPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prices =  PlanPricing::all(); 
        return view('admin/planprice/view_planprice', compact('prices'));
    }

    public function apiShowAll()
    {
        $prices =  PlanPricing::all(); 

        return response()->json([
            'response' => 1,
            'data' => $prices,
        ], 200);
    }

    public function listPrice() {
        $prices =  PlanPricing::all(); 
        $prices_json = json_encode($prices);
        return $prices_json;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $price =  PlanPricing::find($id);

        return view('admin/planprice/edit_planprice', compact('price'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'=>'required',
            'price'=>'required|numeric',
            'category'=>'required',
            'product'=>'required',
            'report'=>'required',
            'button_label'=>'required',
        ]);

        $price =  PlanPricing::find($id);
        $price->title = $request->get('title');
        $price->price = $request->get('price');
        $price->category = $request->get('category');
        $price->product = $request->get('product');
        $price->report = $request->get('report');
        $price->button_label = $request->get('button_label');
        $price->description = $request->get('description');
        $price->save();
    
        return redirect('plan_prices')->with('success', 'Price Plan has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
