<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Store Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,,500,600,700" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('theme_cohost/css/open-iconic-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme_cohost/css/animate.css') }}">
    
    <link rel="stylesheet" href="{{ asset('theme_cohost/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme_cohost/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme_cohost/css/magnific-popup.css') }}">

    <link rel="stylesheet" href="{{ asset('theme_cohost/css/aos.css') }}">

    <link rel="stylesheet" href="{{ asset('theme_cohost/css/ionicons.min.css') }}">

    <link rel="stylesheet" href="{{ asset('theme_cohost/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('theme_cohost/css/jquery.timepicker.css') }}">

    
    <link rel="stylesheet" href="{{ asset('theme_cohost/css/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('theme_cohost/css/icomoon.css') }}">
    <link rel="stylesheet" href="{{ asset('theme_cohost/css/style.css') }}">
  </head>
  <body>
    @include('guest.templates.navigation')

    @yield('content')

    @include('guest.templates.footer')
    @include('guest.templates.script')
  </body>
</html>