@extends('user.layouts.layout')

@section('content')

        <!-- Begin Page Content -->
        <div class="container-fluid">
            @if(session()->get('success'))
              <div class="alert alert-success">
                {{ session()->get('success') }}  
              </div><br />
            @endif
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Contacts</h1>

          <?php
              $contact_size = $contacts->total();
          ?>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Contact: {{$contact_size}}</h6>
                  <div class="text-right">
                      <a class="btn btn-primary" href="{{ route('contacts.create') }}">
                        {{ __('Add Contact') }}
                      </a>
                  </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  @if($contact_size > 0)
                  <thead align="center">
                    <tr>
                      <th>No.</th>
                      <th>Name</th>
                      <th>Role</th>
                      <th>Telephone</th>
                      <th>Description</th>
                      <th colspan="2">Action</th>
                    </tr>
                  </thead>
                  <tbody align="center">
                    <?php 
                        $count = $contacts->firstItem();
                    ?>
                      @foreach($contacts as $contact)
                        <tr>
                          <td>{{ $count++ }}</td>
                          <td>{{ $contact->name }}</td>
                          <td>{{ $contact->role }}</td>
                          <td>{{ $contact->telephone }}</td>
                          <td>{{ $contact->description }}</td>
                          <td>
                              <a class="btn btn-warning" href="{{ route('contacts.edit', $contact->id) }}">
                                  {{ __('Update') }}
                              </a>
                          </td>
                          <td>
                              <form action="{{ route('contacts.destroy', $contact->id) }}" method="POST">
                                  @method('DELETE')
                                  @csrf
                                  <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')">
                                      {{ __('Delete') }}
                                  </button>
                              </form>
                          </td>
                        </tr>
                      @endforeach
                      @while(($count-1) % 5 != 0)
                      <tr>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                      </tr>
                      <?php $count++ ?>
                      @endwhile
                  </tbody>
                  @else
                      <p>You have 0 contact!</p>
                  @endif
                </table>
                  <div class="text-right">
                      {{ $contacts->onEachSide(5)->links() }}
                  </div>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->
@endsection