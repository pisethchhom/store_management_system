<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'isSell',
        'total_price',
        'contact_id',
        'store_id'
    ];
}
