<?php

    use App\Category;

?>
@extends('auth.layouts.layout')

@section('content')
<div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">{{ __('Update Product!') }}</h1>
                  </div>
                  <form method="POST" action="{{ route('products.update', $product->id) }}" class="user">
                        @csrf
                        @method('PATCH')
                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" name="name"
                         id="exampleFirstName" placeholder="Product Name" value="{{ $product->name }}" required autocomplete="name" autofocus>
                    </div>
                    <?php
                        $categories = Category::all();
                    ?>    
                    <div class="form-group form-row">
                        <div class="form-group ml-2 mr-4 col-md-4">
                          <label for="inputState">Category</label>
                          <select name="category_id" id="inputState" class="form-control">
                              @foreach($categories as $category)
                                <option value="<?php echo($category->id) ?>"
                                  @if($category->id == $product->category_id)
                                      selected
                                  @endif>
                                  {{ $category->name }}</option>
                              @endforeach
                          </select>
                          <a class="small" href="{{ route('categories.create') }}">New Category ?</a>
                        </div>
                        <div class="form-group ml-4 mr-4 pt-4 col-md-4">
                            <input type="text" class="form-control form-control-user" name="amount"
                            id="exampleFirstName" placeholder="Product Amount" value="{{ $product->amount }}" autofocus>
                        </div>
                    </div>

                    <div class="form-group form-row">
                        <div class="input-group mr-4 ml-2 mb-4 col-md-4">
                          <div class="input-group-prepend">
                            <span class="input-group-text text-danger" id="validationTooltipUsernamePrepend">$</span>
                          </div>
                          <input type="text" class="form-control form-control-user" id="exampleFirstName" name="price_in"
                          placeholder="Buy in Price" aria-describedby="validationTooltipUsernamePrepend" value="{{ $product->price_in }}"
                          required autofocus>
                        </div>
                        <div class="input-group ml-4 mr-4 mb-4 col-md-4">
                          <div class="input-group-prepend">
                            <span class="input-group-text text-success" id="validationTooltipUsernamePrepend">$</span>
                          </div>
                          <input type="text" class="form-control form-control-user" id="exampleFirstName" name="price_out"
                          placeholder="Sell out Price" aria-describedby="validationTooltipUsernamePrepend" value="{{ $product->price_out }}"
                          required autofocus>
                        </div>
                    </div>

                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" name="measure_unit"
                          id="exampleFirstName" placeholder="Measure Unit (Can, Package, Liter...)" value="{{ $product->measure_unit }}" required autofocus>
                    </div>
                    
                    <input type="hidden" class="form-control form-control-user" name="image_path"
                        id="exampleFirstName" placeholder="Product Path" value="{{ $product->image_path }}" required autofocus>
                        
                    <hr>

                    <button type="submit" class="btn btn-primary btn-user btn-block">
                        {{ __('Update') }}
                    </button>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    
      </div>
<script>
  function loadImage(file) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $('#imageFile').attr('src', e.target.result).height("200px");
    }
    reader.readAsDataURL(file);
  }
</script>
@endsection
