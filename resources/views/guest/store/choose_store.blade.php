<?php

    use App\PlanPricing;
    $plan_price = PlanPricing::all();

?>
@extends('auth.layouts.layout')

@section('content')
<div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <section class="pricing py-5">
                <div class="container">
                    <div class="text-center">
                        <h1 class="h1 text-white mb-4">{{ __('Pick a Plan!') }}</h1>
                    </div>
                    <div class="row">
                        {{-- <form method="POST" action="{{ route('stores.store') }}" class="user"> --}}
                        @foreach ($plan_price as $plan)
                            <!-- Free Tier -->
                            <div class="col-lg-3">
                                <div class="card mb-5 mb-lg-0">
                                  <div class="card-body">
                                  <h5 class="card-title text-muted text-uppercase text-center">{{ $plan->title }}</h5>
                                  <h6 class="card-price text-center">${{ $plan->price }}<span class="period">/month</span></h6>
                                    <hr>
                                    <ul class="fa-ul">
                                        <li><span class="fa-li"><i class="fas fa-check"></i></span>{{ $plan->category }} Categories</li>
                                        <li><span class="fa-li"><i class="fas fa-check"></i></span>{{ $plan->product }} Products</li>
                                        <li><span class="fa-li"><i class="fas fa-check"></i></span>{{ $plan->report }} Reports</li>
                                    </ul>
                                    <a href="{{ route('stores.create', ['plan_id'=>$plan->id]) }}" 
                                        class="btn btn-block btn-primary text-uppercase">{{ $plan->button_label }}</a>
                                  </div>
                                </div>
                            </div>
                        @endforeach
                  </div>
                </div>
              </section>
        </div>
    </div>
</div>
@endsection