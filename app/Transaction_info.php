<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction_info extends Model
{
    protected $fillable = [
        'amount',
        'product_id',
        'transaction_id',
        'store_id'
    ];
}
