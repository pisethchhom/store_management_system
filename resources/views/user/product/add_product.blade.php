<?php

    use App\Category;

?>
@extends('auth.layouts.layout')

@section('content')
<div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">{{ __('Add Product!') }}</h1>
                  </div>
                  <form method="POST" action="{{ route('products.store') }}" class="user" enctype="multipart/form-data">
                        @csrf

                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" name="name"
                         id="exampleFirstName" placeholder="Product Name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                    </div>
                    <?php

                      $categories = Category::where('store_id','=',Session::get('store')['store_id'])->get();

                    ?>
                    
                    <div class="form-group form-row">
                        <div class="form-group ml-2 mr-4 col-md-4">
                          <label for="inputState">Category</label>
                          <select name="category_id" id="inputState" class="form-control">
                              @foreach($categories as $category)
                                  <option value="<?php echo($category->id) ?>">{{ $category->name }}</option>
                              @endforeach
                          </select>
                          <a class="small" href="{{ route('categories.create') }}">New Category ?</a>
                        </div>
                        <div class="form-group ml-4 mr-4 pt-4 col-md-4">
                            <input type="text" class="form-control form-control-user" name="amount"
                            id="exampleFirstName" placeholder="Product Amount" value="{{ old('amount') }}" autofocus>
                        </div>
                    </div>

                    <div class="form-group form-row">
                        <div class="input-group mr-4 ml-2 mb-4 col-md-4">
                          <div class="input-group-prepend">
                            <span class="input-group-text text-danger" id="validationTooltipUsernamePrepend">$</span>
                          </div>
                          <input type="text" class="form-control form-control-user" id="exampleFirstName" name="price_in"
                          placeholder="Buy in Price" aria-describedby="validationTooltipUsernamePrepend" value="{{ old('price_in') }}"
                          required autofocus>
                        </div>
                        <div class="input-group ml-4 mr-4 mb-4 col-md-4">
                          <div class="input-group-prepend">
                            <span class="input-group-text text-success" id="validationTooltipUsernamePrepend">$</span>
                          </div>
                          <input type="text" class="form-control form-control-user" id="exampleFirstName" name="price_out"
                          placeholder="Sell out Price" aria-describedby="validationTooltipUsernamePrepend" value="{{ old('price_out') }}"
                          required autofocus>
                        </div>
                    </div>

                    <div class="form-group form-row mb-4">
                      <input type="text" class="form-control form-control-user" name="measure_unit"
                       id="exampleFirstName" placeholder="Measure Unit (Can, Package, Liter...)" value="{{ old('measure_unit') }}" required autofocus>
                    </div>
                    
                    <div class="form-group form-row">
                      <div class="input-group mr-4 mb-4 pt-2 col-md-4">
                        <div class="custom-file">
                          <input type="file" accept="image/*" onchange="displayImage(this);" class="custom-file-input" id="inputGroupFile01" name="image_path">
                          <label id="inputFileText" class="custom-file-label" for="inputGroupFile01">Choose image file</label>
                        </div>
                      </div>
                      <div class="input-group mr-4 col-md-4">
                        <img src="" class="rounded" id="imageFile" alt="...">
                      </div>
                    </div>
                    <hr>

                    <button type="submit" class="btn btn-primary btn-user btn-block">
                        {{ __('Add') }}
                    </button>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    
      </div>
@endsection

<script>
function displayImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $('#imageFile').attr('src', e.target.result).height("200px");
        }
        document.getElementById('inputFileText').innerHTML = input.value.replace(/.*[\/\\]/, '');
        reader.readAsDataURL(input.files[0]);
    }
}
</script>
