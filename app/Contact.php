<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'name',
        'role',
        'telephone',
        'description',
        'store_id'
    ];
}
