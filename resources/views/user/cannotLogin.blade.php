<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 50vh;
                margin: 0;
            }
            .full-height {
                height: 50vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .center {
                margin: auto;
                width: 60%;
                border: 3px solid #73AD21;
                padding: 10px;
                margin-top: 50px;
                }
            .links > a {
                            padding: 0 25px;
                            color: red;
                        }
</style>
</head>
<body>
    <div class="flex-center position-ref full-height">
    <h1> Sorry! you can not Login with this account.</h1>
    </div>

    <div class="flex-center">
    <p>Please contact our admin</p>
    <div class="links">
        <a class="btn btn-primary" href="{{ route('logout') }}"
          onclick="event.preventDefault();
                    
                    document.getElementById('logout-form').submit();">
           {{ __('GO BACK') }}
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>
    </div>
    
   
    </div>
</body>
</html>