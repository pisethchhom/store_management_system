<?php

namespace App\Http\Controllers;
use App\Transaction_info;
use App\Http\Resources\TransactionInfoResource;
use Illuminate\Http\Request;
use PDF;
class TransactionInfoController extends Controller
{

    public function apiShow(Transaction_Info $transaction_info) : TransactionInfoResource 
    {
        return new TransactionInfoResource($transaction_info);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction_info  $Transaction_info
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

   

    function generatePDF()
    {
        // $data = [
        //     'foo' => 'bar'
        // ];
        $pdf = PDF::loadView('user.transaction.add_transaction');
        return $pdf->stream('document.pdf');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction_info  $Transaction_info
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    public function destroy($id)
    {
        $Transaction_info = Transaction_info::find($id);
        $Transaction_info->delete();
    }
}
