<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\Http\Resources\ProductResource;
use Illuminate\Http\Request;
use Session;
use Response;
use Validator;
use File;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::where('products.store_id', '=', Session::get('store')['store_id'])
            ->join('categories', 'products.category_id', '=', 'categories.id')
            ->select(
                'products.*',
                'categories.name as category'
            )
            ->paginate(5);
        return view('user/product/view_product', compact('products'));
    }

    public function apiShowAll(Request $request)
    {
        $request->validate([
            'store_id' => 'required',
        ]);

        $products = Product::where('store_id', '=', $request->get('store_id'))->get();

        return response()->json([
            'response' => 1,
            'data' => $products,
        ], 200);
    }

    public function apiShowByCategory($id)
    {
        $products = Product::where('category_id', '=', $id)->get();

        return response()->json([
            'response' => 1,
            'data' => $products,
        ], 200);
    }

    public function apiShow($id)
    {
        $product = Product::where('id', '=', $id)->get();

        return response()->json([
            'response' => 1,
            'data' => $product,
        ], 200);
    }

    public function apiAdd(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'category_id' => 'required',
            'amount' => 'required|integer',
            'price_in' => 'required|numeric',
            'price_out' => 'required|numeric',
            'measure_unit' => 'required',
            'store_id' => 'required',
        ]);

        $category = Category::find($request->get('category_id'));

        if ($category) {
            if ($category->store_id != $request->get('store_id')) {
                return response()->json([
                    'response' => 0,
                    'message' => 'Invalid category id',
                ], 401);
            }

            $photo = $request->file('image_path');
            $imageName = "";

            if ($photo) {
                $uid = auth()->user()->id;
                // $path = storage_path('images//' . $uid); //window
                $path = storage_path('images/' . $uid);  //mac
                $fileArray = array('image' => $photo);
                $rules = array('image' => 'mimes:jpeg,jpg,png,gif|required|max:10000');

                $validator = Validator::make($fileArray, $rules);

                if ($validator->fails()) {
                    return response()->json([
                        'response' => 0,
                        'error' => $validator->errors()->getMessages(),
                    ], 400);
                }

                $imageName = sha1(time()) . "." . $photo->getClientOriginalExtension();
                $photo->move($path, $imageName);
            }

            $product = new product([
                'name' => $request->get('name'),
                'category_id' => $request->get('category_id'),
                'store_id' => $request->get('store_id'),
                'amount' => $request->get('amount'),
                'price_in' => $request->get('price_in'),
                'price_out' => $request->get('price_out'),
                'measure_unit' => $request->get('measure_unit'),
                'image_path' => $imageName,
            ]);
            $product->save();

            return response()->json([
                'response' => 1,
                'message' => 'Product has been added',
                'data' => $product,
            ], 200);
        }

        return response()->json([
            'response' => 0,
            'message' => 'Invalid category id',
        ], 401);
    }

    public function apiUpdate(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'name' => 'required',
            'category_id' => 'required',
            'amount' => 'required|integer',
            'price_in' => 'required|numeric',
            'price_out' => 'required|numeric',
            'measure_unit' => 'required',
            'image_path' => 'required',
            'store_id' => 'required',
        ]);

        $product = Product::find($request->get('id'));

        if ($product) {
            if ($product->store_id != $request->get('store_id')) {
                return response()->json([
                    'response' => 0,
                    'message' => 'Invalid store id',
                ], 401);
            }

            $category = Category::find($request->get('category_id'));

            if ($category) {
                if ($category->store_id != $request->get('store_id')) {
                    return response()->json([
                        'response' => 0,
                        'message' => 'Invalid category id',
                    ], 401);
                }

                $product->name = $request->get('name');
                $product->category_id = $request->get('category_id');
                $product->amount = $request->get('amount');
                $product->price_in = $request->get('price_in');
                $product->price_out = $request->get('price_out');
                $product->image_path = $request->get('image_path');
                $product->save();

                return response()->json([
                    'response' => 1,
                    'message' => 'Product has been updated',
                    'data' => $product,
                ], 200);
            }
            return response()->json([
                'response' => 0,
                'message' => 'Invalid category id',
            ], 401);
        } else {
            return response()->json([
                'response' => 0,
                'message' => 'Invalid product id',
            ], 401);
        }
    }

    public function apiDelete(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'store_id' => 'required',
        ]);

        $product = Product::find($request->get('id'));

        if ($product) {
            if ($product->store_id != $request->get('store_id')) {
                return response()->json([
                    'response' => 0,
                    'message' => 'Invalid store id',
                ], 401);
            }

            $product->delete();
            return response()->json([
                'response' => 1,
                'message' => 'Product has been deleted',
            ], 200);
        }

        return response()->json([
            'response' => 0,
            'message' => 'Invalid product id',
        ], 401);
    }

    // test image
    public function apiShowImage($image)
    {
        $uid = auth()->user()->id;
        // $path = storage_path('images\\' . $uid . '\\' . $image); //window
        $path = storage_path('images/' . $uid . '/' . $image);  //mac
        // dd(    $path);

        if (file_exists($path)) {
            return response()->file($path);
        }

        return response()->json([
            'response' => 0,
            'message' => 'Image does not exist',
        ], 404);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user/product/add_product');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request      $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'category_id' => 'required',
            'amount' => 'required|integer',
            'price_out' => 'required|numeric',
            'measure_unit' => 'required',
        ]);

        $photo = $request->file('image_path');
        $imageName = "";

        if ($photo) {
            $uid = auth()->user()->id;
            // $path = storage_path('images\\' . $uid); //window
            $path = storage_path('images/' . $uid); //mac
            $fileArray = array('image' => $photo);
            $rules = array('image' => 'mimes:jpeg,jpg,png,gif|required|max:10000');

            $validator = Validator::make($fileArray, $rules);

            if ($validator->fails()) {
                return response()->json([
                    'response' => 0,
                    'error' => $validator->errors()->getMessages(),
                ], 400);
            }

            $imageName = sha1(time()) . "." . $photo->getClientOriginalExtension();
            $photo->move($path, $imageName);
        }

        $product = new product([
            'name' => $request->get('name'),
            'category_id' => $request->get('category_id'),
            'store_id' => Session::get('store')['store_id'],
            'amount' => $request->get('amount'),
            'price_in' => $request->get('price_in'),
            'price_out' => $request->get('price_out'),
            'measure_unit' => $request->get('measure_unit'),
            'image_path' => $imageName,
        ]);
        $product->save();
        return redirect('products')->with('success', 'Product has been added');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product            $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);

        return view('user/product/edit_product', compact('product'));
    }

    public function search(Request $request)
    {
        $searchData = $request->searchData;

        // start query for search
        $data = Product::where('products.name', 'like', '%', $searchData, '%')
            ->join('categories.name as category')
            ->select('products.*', 'categories.name as category');
        return view('user/product/view_product', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request                     $request
     * @param  \App\Product                     $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'category_id' => 'required',
            'amount' => 'required|integer',
            'price_out' => 'required|numeric',
            'measure_unit' => 'required',
        ]);

        $product = Product::find($id);
        $product->name = $request->get('name');
        $product->category_id = $request->get('category_id');
        $product->amount = $request->get('amount');
        $product->price_in = $request->get('price_in');
        $product->price_out = $request->get('price_out');
        $product->image_path = $request->get('image_path');
        $product->save();

        return redirect('products')->with('success', 'Product has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product                     $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();

        return redirect('products')->with('success', 'Product has been deleted Successfully');
    }
}