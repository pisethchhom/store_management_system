<?php
    use App\Contact;
    use App\Product;

    $contacts = Contact::where('store_id','=',Session::get('store')['store_id'])->get();
    $products = Product::where('store_id','=',Session::get('store')['store_id'])->get();
?>
@extends('auth.layouts.layout')

@section('content')
<style>
  a.disabled {
    color: currentColor;
    cursor: not-allowed;
    opacity: 0.5;
    text-decoration: none;
  }
</style>

<div class="container">
        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            @if($errors->any())
              <div class="alert alert-danger">
                  Error: {{$errors->first()}}!
              </div>
            @endif
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">{{ __('Add Transaction!') }}</h1>
                  </div>
                  
                  <form method="POST" action="{{ route('transactions.store') }}" class="user" onsubmit="addTotalPrice()" id="submit_form">
                        @csrf

                    <div class="form-group col-md-4">
                        <label for="type_choose">Type:</label>
                        <div class="btn-group btn-group-toggle" data-toggle="buttons" id="type_choose">
                          <label class="btn btn-outline-success active">
                            <input type="radio" name="options" id="option1" value="sell_out" checked>Sell out
                          </label>
                          <label class="btn btn-outline-info">
                            <input type="radio" name="options" id="option2" value="buy_in"> Buy in
                          </label>
                        </div>
                    </div>

                    <div class="form-group col-md-4">
                        <div class="form-group row-md-4">
                          <label for="contact_choose">To: </label>
                          <select name="contact_id" id="contact_choose" class="form-control custom-select">
                                <option value="">(No Name)</option>
                                @foreach($contacts as $contact)
                                    <option value="<?php echo($contact->id) ?>">{{ $contact->name }} ({{$contact->role}})</option>
                                @endforeach
                          </select>
                        </div>
                        <div class="form-group row-md-2">
                          <a class="small" href="{{ route('contacts.create') }}">New contact ?</a>
                        </div>
                    </div>

                    <table class="table" style="table-layout: fixed;">
                        <thead align="center">
                          <tr>
                            <th scope="col">Product Name</th>
                            <th scope="col">Product Amount</th>
                            <th scope="col">Price Per Product</th>
                            <th scope="col">
                                <label class="btn btn-info">
                                  <a onclick="addProduct()" id="btn_add_product">Add Product</a>
                                </label>
                            </th>
                          </tr>
                        </thead>
                        <tbody id="tbody_product" align="center">
                        </tbody>
                        <tfoot>
                          <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th id="total_price" scope="col">Total Price: $0</th>
                          </tr>
                        </tfoot>
                    </table>

                    <hr>

                    <button type="submit" class="btn btn-primary btn-user btn-block">
                        {{ __('Add') }}
                    </button>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <script>
        var counter = 0;
        var products = <?php echo json_encode($products); ?>;
        var productSize = <?php echo(sizeof($products)); ?>;

        var option_sell = document.getElementById('option1');
        option_sell.onchange = function () {
          var total = 0;
          for (var i=1; i<=counter; i++) {
            var element = JSON.parse(document.getElementById(`select_product_${i}`).value);
            var elementIndex = element.index;
            var elementPrice = products[element.index].price_out;

            var productPrice = document.getElementById(`product_price_${i}`);
            productPrice.innerHTML = `$${elementPrice}`;
            
            var elementAmount = parseInt(document.getElementById(`input_amount_${i}`).value);
            total += elementPrice * elementAmount;
          }
          document.getElementById("total_price").innerHTML = `Total Price: $${total}`;
        };

        var option_buy = document.getElementById('option2');
        option_buy.onchange = function () {
          var total = 0;
          for (var i=1; i<=counter; i++) {
            var element = JSON.parse(document.getElementById(`select_product_${i}`).value);
            var elementIndex = element.index;
            var elementPrice = products[element.index].price_in;

            var productPrice = document.getElementById(`product_price_${i}`);
            productPrice.innerHTML = `$${elementPrice}`;
            
            var productAmount = document.getElementById(`input_amount_${i}`);
            productAmount.removeAttribute("max");

            total += elementPrice * parseInt(productAmount.value);
            
          }
          document.getElementById("total_price").innerHTML = `Total Price: $${total}`;
        };

        function addProduct() {
          if (counter >= productSize) {
            var btn_add = document.getElementById("btn_add_product");
            btn_add.className = "disabled";
            return ;
          }

          var num = ++counter;
          
          var node = document.createElement("tr");
          var id = `list_product_${num}`;
          node.id = id;

          // ** td_product_choose
          var child_td_product = document.createElement("td");
          var child_product_element = document.createElement("div");
          child_product_element.className = "btn-group btn-group-toggle";
          child_product_element.id = "product_choose";

          var select_td_product = document.createElement("select");
          select_td_product.name = "product_name[]";
          select_td_product.id=`select_product_${num}`;
          select_td_product.className = "form-control custom-select";
          select_td_product.onchange = function () {
                                        var element = JSON.parse($(this).val());
                                        var elementIndex = element.index;
                                        var productPrice = document.getElementById(`product_price_${num}`);
                                        var productAmount = document.getElementById(`input_amount_${num}`);

                                        productPrice.innerHTML = `$${products[elementIndex].price_out}`;
                                        productAmount.max = products[elementIndex].amount;

                                        var total = 0;
                                        for (var i=1; i<=counter; i++) {
                                          var element = JSON.parse(document.getElementById(`select_product_${i}`).value);
                                          var elementPrice = products[element.index].price_out;
                                          var elementAmount = parseInt(document.getElementById(`input_amount_${i}`).value);
                                          total += elementPrice * elementAmount;
                                        }
                                        document.getElementById("total_price").innerHTML = `Total Price: $${total}`;
                                      };

          var first_product_price = products[0].price_out;
          
          for (var i=0; i<products.length; i++) {
              var value = document.createElement("option");
              value.text = products[i].name;
              var value_object = {
                id : products[i].id,
                index : i
              };
              value.value = JSON.stringify(value_object);
              select_td_product.options.add(value);
          }

          child_product_element.appendChild(select_td_product);
          child_td_product.appendChild(child_product_element);
          node.appendChild(child_td_product);

          // ** td_product_amount
          var child_td_amount = document.createElement("td");
          var child_amount_element = document.createElement("div");
          child_amount_element.className = "btn-group btn-group-toggle";
          child_amount_element.id = "product_amount";

          // add input field for product amount 
          var child_amount_input = document.createElement("input");
          child_amount_input.type = "number";
          child_amount_input.id = `input_amount_${num}`;
          child_amount_input.className = "form-control";
          child_amount_input.name = "product_amount[]";
          child_amount_input.defaultValue = 0;
          child_amount_input.placeholder = "Product Amount";
          child_amount_input.required = true;
          child_amount_input.autofocus = true;
          child_amount_input.min = 0;
          child_amount_input.max = products[0].amount;
          child_amount_input.oninput = function () {
            var total = 0;
            for (var i=1; i<=counter; i++) {
              var element = JSON.parse(document.getElementById(`select_product_${i}`).value);
              var elementPrice = products[element.index].price_out;
              var elementAmount = parseInt(document.getElementById(`input_amount_${i}`).value);
              total += elementPrice * elementAmount;
            }
            document.getElementById("total_price").innerHTML = `Total Price: $${total}`;
          };
          
          child_amount_element.appendChild(child_amount_input);
          child_td_amount.appendChild(child_amount_element);
          node.appendChild(child_td_amount);

          // ** td_product_price
          var child_td_price = document.createElement("td");
          child_td_price.id = `product_price_${num}`;
          child_td_price.innerHTML = `$${first_product_price}`;
          node.appendChild(child_td_price);

          // ** td_product_action
          var child_td_action = document.createElement("td");
          var child_action_element = document.createElement("label");
          child_action_element.className = "btn btn-danger btn-circle";

          var child_action_click = document.createElement("A");
          child_action_click.onclick = function () {
                                          node.parentElement.removeChild(node);
                                          --counter;
                                          var btn_add = document.getElementById("btn_add_product");
                                          btn_add.className = "";
                                      };
          var child_action_icon = document.createElement("i");
          child_action_icon.className = "fas fa-times";
          
          child_action_click.appendChild(child_action_icon);
          child_action_element.appendChild(child_action_click);
          child_td_action.appendChild(child_action_element);
          node.appendChild(child_td_action);

          document.getElementById("tbody_product").appendChild(node);
        }

        function addTotalPrice() {
          var totalPrice = document.getElementById("total_price").innerHTML;
          var totalPriceInput = document.createElement("input");
          totalPriceInput.type = "hidden";
          totalPriceInput.name = "total_price";
          totalPriceInput.value = Number(totalPrice.replace(/[^0-9\.]+/g,""));;
          document.getElementById("submit_form").appendChild(totalPriceInput);
        };

        

        function isOnSellOut(isSellOut) {
          var total = 0;
          for (var i=1; i<=counter; i++) {
            var element = JSON.parse(document.getElementById(`select_product_${i}`).value);
            var elementIndex = element.index;
            var elementPrice = isSellOut ? products[element.index].price_out : products[element.index].price_in;

            var productPrice = document.getElementById(`product_price_${i}`);
            productPrice.innerHTML = `$${elementPrice}`;
            
            var elementAmount = parseInt(document.getElementById(`input_amount_${i}`).value);
            total += elementPrice * elementAmount;
          }
          document.getElementById("total_price").innerHTML = `Total Price: $${total}`;
        }
    </script>
@endsection