<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('plan_pricings')->insert([
            'price' => 0,
            'title' => 'Free',
            'category' => '5',
            'product' => '20',
            'report' => '6 Months',
            'description' => '100% free. Forever',
            'button_label' => 'Try Free',
        ]);
        DB::table('plan_pricings')->insert([
            'price' => 5,
            'title' => 'Silver',
            'category' => '10',
            'product' => '50',
            'report' => '10 Months',
            'description' => 'Good for Startup',
            'button_label' => 'Choose Plan',
        ]);
        DB::table('plan_pricings')->insert([
            'price' => 20,
            'title' => 'Gold',
            'category' => '25',
            'product' => '200',
            'report' => '2 Years',
            'description' => 'Good for Small Business',
            'button_label' => 'Choose Plan',
        ]);
        DB::table('plan_pricings')->insert([
            'price' => 99,
            'title' => 'Pro',
            'category' => 'Unlimited',
            'product' => 'Unlimited',
            'report' => 'Unlimited',
            'description' => 'Unlock the Limitation',
            'button_label' => 'Choose Plan',
        ]);

        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin12345'),
            'isAdmin' => 1,
            'isActive' => 1,
        ]);
    }
}
