@extends('auth.layouts.layout')

@section('content')
<div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">{{ __('Add Category!') }}</h1>
                  </div>
                  <form method="POST" action="{{ route('categories.store') }}" class="user">
                        @csrf

                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" name="name"
                         id="exampleFirstName" placeholder="Category Name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                    </div>

                    <div class="form-group">
                      <textarea rows="4" cols="50" name="description" class="form-control" placeholder="Category Description..."></textarea>
                    </div>
                    
                    <hr>

                    <button type="submit" class="btn btn-primary btn-user btn-block">
                        {{ __('Add') }}
                    </button>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    
      </div>
@endsection
