<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(auth()->user()->isAdmin == 1){
            return view('admin.dashboard'); 
        }

        if(auth()->user()->isActive ==1 ){
            $store_query = Store::where('user_id','=',auth()->user()->id)
            ->join('plan_pricings', 'stores.plan_id', '=', 'plan_pricings.id')
            ->select('stores.id as store_id', 'stores.name as store_name', 'stores.expire_plan', 'stores.isActive',
            'plan_pricings.title as plan_title',
            'plan_pricings.category as category_limit',
            'plan_pricings.product as product_limit',
            'plan_pricings.report as report_limit')->get();
            $store = $store_query->toArray();
        }
        else{
            return view('user.cannotLogin');
        }
        
        if (sizeof($store) != 0) {
            $store = $store[0];
            Session::put('store', $store);

            if (Session::get('store')['isActive']) {
                return view('user.dashboard');
            }
            return view('guest/store/activate_store');
        }
        return view('guest/store/choose_store');
    }
}
