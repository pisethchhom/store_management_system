@extends('auth.layouts.layout')

@section('content')
<div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <section class="pricing py-5">
                <div class="container">
                    <div class="text-center">
                        <h1 class="h1 text-white mb-4">{{ __('Activate Store') }}</h1>
                    </div>
                    <div class="row">
                        <a href="{{ URL('stores/'.Session::get('store')['store_id'].'/activate') }}" 
                            class="btn btn-block btn-primary text-uppercase">{{ __("Activate My Store") }}
                        </a>
                  </div>
                </div>
              </section>
        </div>
    </div>
</div>
@endsection