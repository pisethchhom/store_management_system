@extends('user.layouts.layout')

@section('content')

        <!-- Begin Page Content -->
        <div class="container-fluid">
            @if(session()->get('success'))
              <div class="alert alert-success">
                {{ session()->get('success') }}  
              </div><br />
            @endif
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Categories</h1>
          <?php

              $category_size = $categories->total();
              if(Session::get('store')['category_limit'] == "Unlimited") {
                $category_label = "Unlimited";
                $category_left = 1;
              }
              else {
                $category_left = Session::get('store')['category_limit'] - $category_size;
                $category_label = ($category_left == 0) ? "full" : $category_left." left";
              }

          ?>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Category: {{ $category_label }}</h6>
                  <div class="text-right">
                    @if(($category_left > 0) || ($category_label == "Unlimited")) 
                      <a class="btn btn-primary" href="{{ route('categories.create') }}">
                        {{ __('Add Category') }}
                      </a>
                    @else 
                      <a class="btn btn-success" href="#full">
                        {{ __('Category is full') }}
                      </a>
                    @endif
                  </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  @if($category_size > 0)
                  <thead align="center">
                    <tr>
                      <th>No.</th>
                      <th>Name</th>
                      <th>Description</th>
                      <th colspan="2">Action</th>
                    </tr>
                  </thead>
                  <tbody align="center">
                    <?php 
                        $count = $categories->firstItem();
                    ?>
                    @foreach($categories as $category)
                      <tr>
                        <td>{{ $count++ }}</td>
                        <td>{{ $category->name }}</td>
                        <td>{{ $category->description }}</td>
                        <td>
                            <a class="btn btn-warning" href="{{ route('categories.edit', $category->id) }}">
                                {{ __('Update') }}
                            </a>
                        </td>
                        <td>
                            <form class="delete" action="{{ route('categories.destroy', $category->id) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')">
                                    {{ __('Delete') }}
                                </button>
                            </form>
                        </td>
                      </tr>
                      @endforeach
                      @while(($count-1) % 5 != 0)
                      <tr>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                      </tr>
                      <?php $count++ ?>
                      @endwhile
                  </tbody>
                  @else
                      <p>You have 0 category!</p>
                  @endif
                </table>
                  <div class="text-right">
                      {{ $categories->onEachSide(5)->links() }}
                  </div>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->
@endsection