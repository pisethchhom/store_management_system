@if(Session::has('store'))
    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('/home') }}">
          <div class="sidebar-brand-icon">
            <i class="fas fa-store"></i>
          </div>
          <div class="sidebar-brand-text mx-3">{{ Session::get('store')['store_name'] }}</div>
        </a>
  
        <!-- Divider -->
        <hr class="sidebar-divider my-0">
        
        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
          <a class="nav-link" href="{{ url('/home') }}">
            <i class="fas fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="{{ route('transactions.index') }}">
            <i class="fas fa-fw fa-exchange-alt"></i>
            <span>Transactions</span></a>
        </li>
  
        <!-- Divider -->
        <hr class="sidebar-divider">
  
        <!-- Heading -->
        <div class="sidebar-heading">
          People
        </div>

        <!-- Nav Item - Charts -->
        <li class="nav-item">
            <a class="nav-link" href="{{ route('contacts.index') }}">
                <i class="fas fa-fw fa-id-badge"></i>
                <span>Contact</span></a>
            </a>
        </li>

        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Store
        </div>

        <!-- Nav Item - Charts -->
        <li class="nav-item">
            <a class="nav-link" href="{{ route('categories.index') }}">
                <i class="fas fa-fw fa-boxes"></i>
                <span>Category</span></a>
            </a>
        </li>

        <!-- Nav Item - Charts -->
        <li class="nav-item">
            <a class="nav-link" href="{{ route('products.index') }}">
                <i class="fas fa-fw fa-box-open"></i>
                <span>Product</span></a>
            </a>
        </li>
        
        <!-- Nav Item - Charts -->
        <li class="nav-item">
          <a class="nav-link active" href="{{ url('user/report') }}">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Reports</span></a>
        </li>
        
  
        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">
  
        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
          <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>
      </ul>
@else
<?php
redirect()->action('StoreController@chooseStore');
?>
@endif