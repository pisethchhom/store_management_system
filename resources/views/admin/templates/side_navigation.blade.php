        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <div class="nav-left-sidebar sidebar-dark">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            <li class="nav-divider">
                                Admin
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="#"><i class="fas fa-tachometer-alt"></i>Dashboard</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('stores.index') }}"><i class="fas fa-store"></i>Manage Store</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('plan_prices.index') }}"><i class="fas fa-tag"></i>Manage Price</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('users.index') }}"><i class="fa fa-fw fa-user-circle"></i>Manage Users</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->