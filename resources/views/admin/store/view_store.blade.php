@extends('admin.layouts.layout')

@section('content')
@if(\Session::has('error'))

<div class="alert alert-danger">
  {{\Session::get('error')}}
</div>

@elseif(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}  
     </div><br />
@endif
<div class="dashboard-ecommerce">
    <div class="container-fluid dashboard-content ">
        <!-- ============================================================== -->
        <!-- pageheader  -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Store</h2>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader  -->
        <!-- ============================================================== -->
        <div class="ecommerce-widget">
            <div class="row">
                <!-- ============================================================== -->
          
                <!-- ============================================================== -->

                              <!-- recent orders  -->
                <!-- ============================================================== -->
                <div class="col">
                    <div class="card">
                        <h5 class="card-header">Recent Orders</h5>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="bg-light" align="center">
                                        <tr class="border-0">
                                            <th class="border-0">#</th>
                                            <th class="border-0">Title</th>
                                            <th class="border-0">Owner</th>
                                            <th class="border-0">Plan Title</th>
                                            <th class="border-0">Description</th>
                                            <th class="border-0">Expire Plan Date</th>
                                            <th class="border-0">Status</th>
                                            <th class="border-0">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody align="center">
                                        @foreach ($stores as $store)
                                          <tr>
                                                <td>{{$store->id}}</td>
                                                <td>{{$store->store_title}}</td>
                                                <td>{{$store->username}}</td>
                                                <td>{{$store->plan_title}}</td>
                                                <td>{{$store->description}}</td>
                                                <td>{{$store->expire_plan}}</td>
                                                <td>
                                                    @if ($store->isActive)
                                                        <span class="badge-dot badge-success mr-1"></span>
                                                        Active
                                                        </td>
                                                        <td>
                                                            <a class="btn btn-danger" href="{{ URL('stores/'.$store->id.'/deactivate') }}">
                                                            {{ __('Deactivate') }}
                                                            </a>
                                                    @else 
                                                        <span class="badge-dot badge-brand mr-1"></span>
                                                        Expired
                                                        </td>
                                                        <td>
                                                            <a class="btn btn-success" href="{{ URL('stores/'.$store->id.'/activate') }}">
                                                            {{ __('Activate') }}
                                                            </a>
                                                    @endif
                                                </td>
                                          </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end recent orders  -->
              </div>
            </div>
        </div>
</div>
@endsection