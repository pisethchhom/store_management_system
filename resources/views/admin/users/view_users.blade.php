@extends('admin.layouts.layout')

@section('content')
@if(\Session::has('error'))

<div class="alert alert-danger">
  {{\Session::get('error')}}
</div>

@elseif(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}  
     </div><br />
@endif

<div class="dashboard-ecommerce">
    <div class="container-fluid dashboard-content ">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Users</h2>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="ecommerce-widget">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <h5 class="card-header">Manage Users</h5>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="bg-light" align="center">
                                        <tr class="border-0">
                                            <th class="border-0">#</th>
                                            <th class="border-0">Name</th>
                                            <th class="border-0">E-mail</th>
                                            <th class="border-0">Status</th> 
                                            <th class="border-0">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody align="center">
                                        @foreach ($users as $user)
                                          <tr>
                                                <td>{{$user->id}}</td>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>
                                                    @if ($user->isActive)
                                                        <span class="badge-dot badge-success mr-1"></span>
                                                        Active
                                                        </td>
                                                        <td>
                                                            @if ($user->isAdmin)
                                                            ---
                                                            @else
                                                                <a class="btn btn-danger" href="{{ URL('users/'.$user->id.'/deactivate') }}">
                                                                {{ __('Deactivate') }}
                                                                </a>
                                                            @endif
                                                    @else 
                                                        <span class="badge-dot badge-brand mr-1"></span>
                                                        Expired
                                                        </td>
                                                        <td>
                                                            @if ($user->isAdmin)
                                                            ---
                                                            @else
                                                                <a class="btn btn-success" href="{{ URL('users/'.$user->id.'/activate') }}">
                                                                {{ __('Activate') }}
                                                                </a>
                                                            @endif
                                                    @endif
                                                </td>
                                          </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
        </div>
</div>
@endsection