@extends('user.layouts.layout')

@section('content')

        <!-- Begin Page Content -->
        <div class="container-fluid">
            @if(session()->get('success'))
              <div class="alert alert-success">
                {{ session()->get('success') }}  
              </div><br />
            @endif
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Transactions</h1>
          <?php
              $transaction_size = $transactions->total();
          ?>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Total Transactions: {{$transaction_size}}</h6>
                  <div class="text-right">
                      <a class="btn btn-primary" href="{{ route('transactions.create') }}">
                        {{ __('Add Transaction') }}
                      </a>
                  </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  @if($transaction_size > 0)
                  <thead align="center">
                    <tr>
                      <th>No.</th>
                      <th>Type</th>
                      <th>To</th>
                      <th>Total Price</th>
                      <th>Date</th>
                      <th colspan="2">Action</th>
                    </tr>
                  </thead>
                  <tbody align="center">
                    <?php 
                        $count = $transactions->firstItem();
                    ?>
                      @foreach($transactions as $transaction)
                        <tr>
                          <td>{{ $count++ }}</td>
                          <td>{{ $transaction->isSell ? "Sell out" : "Buy in" }}</td>
                          <td>{{ $transaction->contact ? $transaction->contact : "(No Name)" }}</td>
                          <td>${{ $transaction->total_price }}</td>
                          <td>{{ $transaction->created_at->format('D-d-M-Y') }}</td>
                          <td>
                              <a class="btn btn-success" href="{{ route('transactions.show', $transaction->id) }}">
                                  {{ __('View') }}
                              </a>
                          </td>
                          <td>
                              <form action="{{ route('transactions.destroy', $transaction->id) }}" method="POST">
                                  @method('DELETE')
                                  @csrf
                                  <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')">
                                      {{ __('Delete') }}
                                  </button>
                              </form>
                          </td>
                        </tr>
                      @endforeach
                      @while(($count-1) % 5 != 0)
                      <tr>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                      </tr>
                      <?php $count++ ?>
                      @endwhile
                  </tbody>
                  @else
                      <p>You have 0 Transaction!</p>
                  @endif
                </table>
                  <div class="text-right">
                      {{ $transactions->onEachSide(5)->links() }}
                  </div>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->
@endsection