@extends('admin.layouts.layout')

@section('content')

<div class="dashboard-ecommerce">
    <div class="container-fluid dashboard-content ">
        <!-- ============================================================== -->
        <!-- pageheader  -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Price Plans</h2>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader  -->
        <!-- ============================================================== -->
        <div class="ecommerce-widget">
            <div class="row">
                <!-- ============================================================== -->
          
                <!-- ============================================================== -->

                              <!-- recent orders  -->
                <!-- ============================================================== -->
                <div class="col">
                    <div class="card">
                        <h5 class="card-header">Recent Orders</h5>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="bg-light" align="center">
                                        <tr class="border-0">
                                            <th class="border-0">#</th>
                                            <th class="border-0">Title</th>
                                            <th class="border-0">Plan Price</th>
                                            <th class="border-0">Category Limited</th>
                                            <th class="border-0">Product Limited</th>
                                            <th class="border-0">Report Limited</th>
                                            <th class="border-0">Description</th>
                                            <th class="border-0">Button Label</th>
                                            <th class="border-0">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody align="center">
                                        @foreach ($prices as $plan)
                                          <tr>
                                              <td>{{$plan->id}}</td>
                                              <td>{{$plan->title}}</td>
                                              <td>${{$plan->price}}</td>
                                              <td>{{$plan->category}}</td>
                                              <td>{{$plan->product}}</td>
                                              <td>{{$plan->report}}</td>
                                              <td>{{$plan->description}}</td>
                                              <td>{{$plan->button_label}}</td>
                                              <td>
                                                  <a class="btn btn-warning" href="{{ route('plan_prices.edit', $plan->id) }}">
                                                      {{ __('Update') }}
                                                  </a>
                                              </td>
                                          </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end recent orders  -->
                <div class="col">
                    <div class="card">
                        <h5 class="card-header">Recent Orders</h5>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="bg-light" align="center">
                                        <tr class="border-0">
                                            <th class="border-0">#</th>
                                            <th class="border-0">Title</th>
                                            <th class="border-0">Plan Price</th>
                                            <th class="border-0">Category Limited</th>
                                            <th class="border-0">Product Limited</th>
                                            <th class="border-0">Report Limited</th>
                                            <th class="border-0">Description</th>
                                            <th class="border-0">Button Label</th>
                                            <th class="border-0">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody align="center">
                                        @foreach ($prices as $plan)
                                          <tr>
                                              <td>{{$plan->id}}</td>
                                              <td>{{$plan->title}}</td>
                                              <td>${{$plan->price}}</td>
                                              <td>{{$plan->category}}</td>
                                              <td>{{$plan->product}}</td>
                                              <td>{{$plan->report}}</td>
                                              <td>{{$plan->description}}</td>
                                              <td>{{$plan->button_label}}</td>
                                              <td>
                                                  <a class="btn btn-warning" href="{{ route('plan_prices.edit', $plan->id) }}">
                                                      {{ __('Update') }}
                                                  </a>
                                              </td>
                                          </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
        </div>
</div>
@endsection