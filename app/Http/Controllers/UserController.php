<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Session;

class UserController extends Controller
{
    public function index()
    {
        $users =  User::all(); 

        return view('admin/users/view_users', compact('users'));
    }

    public function apiShow(User $user) : UserResource 
    {
        return new UserResource($user);
    }

    public function activate($id)
    {
        $users = User::find($id);
        $users->isActive = 1;
        $users->save();

        if(auth()->user()->isAdmin == 1){

            return redirect('users')->with('success', 'User has been activated');
            
        }
        return view('users');
    }

    public function deactivate($id)
    {
        
        $users = User::find($id);
        $users->isActive = 0;
        $users->save();

        return redirect('users')->with('success', 'Store has been deactivated');
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
   
        return redirect('users')->with('success', 'User has been deleted Successfully');
    }


}
